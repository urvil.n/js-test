var assert = require('assert');

const test = [
    {
      "p": 73,
      "r": 98,
      "t": 11
    },
    {
      "p": 25,
      "r": 37,
      "t": 77
    },
    {
      "p": 34,
      "r": -44,
      "t": 69
    },
    {
      "p": 86,
      "r": 25,
      "t": 27
    },
    {
      "p": 27,
      "r": -38,
      "t": 11
    },
    {
      "p": 4,
      "r": 11,
      "t": 66
    }
   ]

   test.filter(x =>  x.r < 0).map(x => x.r=0-x.r);

   test.filter(x =>  x.p < 0).map(x => x.p=0-x.p);
   
   test.filter(x =>  x.t < 0).map(x => x.t=0-x.t);

 
   let answer=[];

   for (const iterable of test) {
     answer.push(interest(iterable));
   }

   function interest(itr)
   {
       return Number((itr.p * itr.r * itr.t / 100).toFixed(2));
   }

   console.log(answer);


   