var voters = [
    {name:'Bob' , age: 30, voted: true},
    {name:'Jake' , age: 32, voted: true},
    {name:'Kate' , age: 25, voted: false},
    {name:'Sam' , age: 20, voted: false},
    {name:'Phil' , age: 21, voted: true},
    {name:'Ed' , age:55, voted:true},
    {name:'Tami' , age: 54, voted:true},
    {name: 'Mary', age: 31, voted: false},
    {name: 'Becky', age: 43, voted: false},
    {name: 'Joey', age: 41, voted: true},
    {name: 'Jeff', age: 30, voted: true},
    {name: 'Zack', age: 19, voted: false}
 ];
 function voterResults(arr) {
   const youth=arr.filter(x => ( x.age >= 18 && x.age <= 25))
   const mids=arr.filter(x => ( x.age >= 26 && x.age <= 35))
   const olds=arr.filter(x => ( x.age >= 36 && x.age <= 55))
   
   const youngvotes=youth.filter(x => x.voted==true)
   const midvotes=mids.filter(x => x.voted==true)
   const oldvotes=olds.filter(x => x.voted==true)

   return {
       youngvotes: youngvotes.length, 
       youth:youth.length,
       midvotes: midvotes.length,
       mids: mids.length,
       oldvotes: oldvotes.length,
       olds: olds.length
    }
 }

 console.log(voterResults(voters));

 